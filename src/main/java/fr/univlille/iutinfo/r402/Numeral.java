package fr.univlille.iutinfo.r402;
public class Numeral {
    private static final String[] units = {"Zéro", "Un", "Deux", "Trois", "Quatre", "Cinq", "Six", "Sept", "Huit", "Neuf"};
    private static final String[] specificsTens = {"Dix", "Onze", "Douze", "Treize", "Quatorze", "Quinze", "Seize"};
    private static final String[] tens = {"", "Dix", "Vingt", "Trente", "Quarante", "Cinquante", "Soixante", "Soixante", "Quatre-vingt", "Quatre-vingt"};
    private String number;
    private int intNumber;
    private boolean casseCouille;

    public Numeral (String number) throws NotAValidInputException {
        try {
            this.number = number;
            this.intNumber = Integer.parseInt(number);
            if(intNumber < 0) throw new NotAValidInputException();
            casseCouille = isCasseCouille(intNumber);
        } catch (NumberFormatException e){
            throw new NotAValidInputException();
        }
    }
    public String getNumber(){return number;}
    public String toLetters () {
        return tens() + units();
    }

    private String tens(){
        return tens(intNumber, casseCouille);
    }

    private String units(){
        return units(intNumber, casseCouille);
    }

    private String units(int val, boolean casseCouille){
        System.out.println(val);
        String s = "";
        if(val > 9 && (val%10 == 0 && !casseCouille)) return s;

        if(casseCouille) {
            if(val > 20){
                int subbed = 10 + val % 10;
                System.out.println(subbed);
                s += tens(subbed, isCasseCouille(subbed)).toLowerCase() + units(subbed, isCasseCouille(subbed));
            } else {
                s += specificsTens[val%10];
            }
        } else {
            s += units[val%10];
        }

        if(val > 16){
            String prefix = val%10 == 1 ? " et " : "-";
            s = prefix + s.toLowerCase();
        }


        return s;
    }

    private String tens(int val, boolean casseCouille){
        int tenIndex = val / 10;
        String s = "";
        if(val > 16 || !casseCouille) s = tens[tenIndex];
        return s;
    }

    private boolean isCasseCouille(int val){
        int tenIndex = val/10;
        return tenIndex%2 == 1 && (tenIndex > 6 || tenIndex < 2) && (val>20 || val%10 < 7) && val != 10;
    }
}
