package fr.univlille.iutinfo.r402;

public class NotAValidInputException extends Exception {
    public NotAValidInputException(){
        super("Constructor parameter is not a number");
    }
}
