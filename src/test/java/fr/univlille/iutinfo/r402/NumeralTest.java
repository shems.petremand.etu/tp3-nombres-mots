package fr.univlille.iutinfo.r402;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class NumeralTest {
    @Test public void does_contain_a_number(){
        assertThrows(NotAValidInputException.class, () -> new Numeral("a"));
        assertThrows(NotAValidInputException.class, () -> new Numeral("-23"));
        assertThrows(NotAValidInputException.class, () -> new Numeral("AGAGAGAG"));
        assertThrows(NotAValidInputException.class, () -> new Numeral("prout"));
        assertDoesNotThrow(() -> new Numeral("5"));
        assertDoesNotThrow(() -> new Numeral("25"));
        assertDoesNotThrow(() -> new Numeral("2343245"));
    }

    @Test public void test_on_units() throws NotAValidInputException {
        Numeral num = new Numeral("1");
        assertEquals("Un", num.toLetters());
        num = new Numeral("2");
        assertEquals("Deux", num.toLetters());
        num = new Numeral("3");
        assertEquals("Trois", num.toLetters());
        num = new Numeral("4");
        assertEquals("Quatre", num.toLetters());
        num = new Numeral("5");
        assertEquals("Cinq", num.toLetters());
        num = new Numeral("6");
        assertEquals("Six", num.toLetters());
        num = new Numeral("7");
        assertEquals("Sept", num.toLetters());
        num = new Numeral("8");
        assertEquals("Huit", num.toLetters());
        num = new Numeral("9");
        assertEquals("Neuf", num.toLetters());
        num = new Numeral("0");
        assertEquals("Zéro", num.toLetters());
    }

    @Test public void test_from_ten_to_twenty() throws NotAValidInputException {
        Numeral num = new Numeral("10");
        assertEquals("Dix", num.toLetters());
        num = new Numeral("11");
        assertEquals("Onze", num.toLetters());
        num = new Numeral("12");
        assertEquals("Douze", num.toLetters());
        num = new Numeral("13");
        assertEquals("Treize", num.toLetters());
        num = new Numeral("14");
        assertEquals("Quatorze", num.toLetters());
        num = new Numeral("15");
        assertEquals("Quinze", num.toLetters());
        num = new Numeral("16");
        assertEquals("Seize", num.toLetters());
        num = new Numeral("17");
        assertEquals("Dix-sept", num.toLetters());
        num = new Numeral("19");
        assertEquals("Dix-neuf", num.toLetters());
        //17 to 19 shall follow the same rules
    }

    @Test public void test_from_twenty_to_thirty() throws NotAValidInputException {
        Numeral num = new Numeral("20");
        assertEquals("Vingt", num.toLetters());
        num = new Numeral("21");
        assertEquals("Vingt et un", num.toLetters());
        num = new Numeral("22");
        assertEquals("Vingt-deux", num.toLetters());
        num = new Numeral("29");
        assertEquals("Vingt-neuf", num.toLetters());
        //22 to 29 shall follow the same rules
    }

    @Test public void other_classic_tens() throws NotAValidInputException {
        Numeral num = new Numeral("30");
        assertEquals("Trente", num.toLetters());
        num = new Numeral("40");
        assertEquals("Quarante", num.toLetters());
        num = new Numeral("50");
        assertEquals("Cinquante", num.toLetters());
        num = new Numeral("60");
        assertEquals("Soixante", num.toLetters());
    }

    @Test public void specific_tens_combination_should_be_recognized() throws NotAValidInputException {
        Numeral num = new Numeral("72");
        assertEquals("Soixante-douze", num.toLetters());
        num = new Numeral("95");
        assertEquals("Quatre-vingt-quinze", num.toLetters());
        num = new Numeral("71");
        assertEquals("Soixante et onze", num.toLetters());
        num = new Numeral("90");
        assertEquals("Quatre-vingt-dix", num.toLetters());
        num = new Numeral("77");
        assertEquals("Soixante-dix-sept", num.toLetters());
    }
}
